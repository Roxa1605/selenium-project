package Selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class SingleAuthorPage {

	
		public WebDriver driver;
		
		
		public SingleAuthorPage(WebDriver driver) {
			this.driver = driver;	
		}
		
		
		public By DramaLink = By.cssSelector("div[class='sc_skills_item sc_skills_style_1 odd first inited']");
		public By BiographyLink = By.cssSelector("div[class='sc_skills_item sc_skills_style_1 even inited']");
		public By CookbooksLink = By.cssSelector("div[class='sc_skills_item sc_skills_style_1 odd inited']");
		
			
		
		public void checkSingleAuthorValues() {
			driver.findElement(DramaLink);
			driver.findElement(BiographyLink);
			driver.findElement(CookbooksLink);
			

			}
}
