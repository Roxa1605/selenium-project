package Selenium.tests;

import org.testng.annotations.Test;

import Selenium.pages.NavigationMenuPage;
import Selenium.utils.BaseTest;

public class NavigationTest extends BaseTest{
	
	
	
	@Test
	public void navigateToContacts() {
		
		NavigationMenuPage menu =  new NavigationMenuPage(driver);
		
		menu.navigateTo(menu.contactLink);
		
		
	}


}
