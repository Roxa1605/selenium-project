package Selenium.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import Selenium.utils.BaseTest;
import Selenium.pages.NavigationMenuPage;
import Selenium.pages.SingleAuthorPage;


public class SingleAuthorTest extends BaseTest {
	

	@Test
	public void SingleAuthorTest() {
		
		NavigationMenuPage menu =  new NavigationMenuPage(driver);
		menu.navigateTo(menu.singleAuthorLink);
		
				
		SingleAuthorPage singleAuthorPage = new SingleAuthorPage(driver);
		singleAuthorPage.checkSingleAuthorValues();
						
		WebElement DramaValue = driver.findElement (By.cssSelector("div[data-stop='95']"));
		String actualDrama = DramaValue.getAttribute("data-stop");
		assertEquals(actualDrama, "95");
		
		WebElement BiographyValue = driver.findElement (By.cssSelector("div[data-stop='75']"));
		String actualBiography = BiographyValue.getAttribute("data-stop");
		assertEquals(actualBiography, "75");
		
		WebElement CookbooksValue = driver.findElement (By.cssSelector("div[data-stop='82']"));
		String actualCookbooks = CookbooksValue.getAttribute("data-stop");
		assertEquals(actualCookbooks, "82");
		
				
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='sc_skills_item sc_skills_style_1 odd inited']")));
		
		
	}

	

	

	
	
}
